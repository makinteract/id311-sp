void setup()
{
	try{
		println (parseInteger("214"));
		println (parseInteger("a214"));
	} catch (NumberFormatException e)
	{
		println("Exception caught: " +e.getMessage() );
	}
}

int parseInteger (String s) throws NumberFormatException
{
	int result = 0;
	int sign = 1;

	// handling the negative sign
	if (s.charAt(0) == '-')
	{
		sign= -1;
		s= s.substring(1, s.length());
	}

	for (int i=0; i<s.length(); i++)
	{
		int n = (int) (s.charAt(i)-'0');
		if (n<0 || n>9) throw new NumberFormatException ("Something went wrong");
		result = (result*10)+n;
	}
	return result*sign;
}