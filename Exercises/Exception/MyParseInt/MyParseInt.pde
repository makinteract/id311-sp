void setup()
{
	println (parseInteger("214"));
}

int parseInteger (String s)
{
	int result = 0;
	int sign = 1;

	// handling the negative sign
	if (s.charAt(0) == '-')
	{
		sign= -1;
		s= s.substring(1, s.length());
	}

	for (int i=0; i<s.length(); i++)
	{
		int n = (int) (s.charAt(i)-'0');
		result = (result*10)+n;
	}
	return result*sign;
}