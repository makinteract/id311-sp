Turtle t;
 
void setup() {
    size(800,600);    
    t = new Turtle();
    t.setxy(-100,0);
    t.right(90);
    noLoop();
}
void draw() {
    background(255);
    koch(t,3,200);
}

void koch (Turtle t, int order, int length)
{
    if (order == 0)          
        t.forward(length);
    else
    {
        koch(t, order-1, length/3);
        t.left(60);
        koch(t, order-1, length/3);
        t.right(120);
        koch(t, order-1, length/3);
        t.left(60);
        koch(t, order-1, length/3);
    }
}