// Constants
final int RINGS=4;
final int REFRESH_DELAY_MS = 100;

// The code
Tower t;

void setup()
{
	size(800, 600);
	t= new Tower(RINGS);
	t.setSpeed(REFRESH_DELAY_MS);
}

void draw()
{
	background(255);	
	t.draw();
}


void mousePressed()
{
	new Thread()
	{
	    public void run() {
	        TOH(RINGS,0,2,1);
	    }
	}.start();
}



void TOH (int n, int start, int end, int helper)
{
	if (n==1)
		move (start,end);
	else
	{
		TOH (n-1, start, helper, end);
		move (start,end);
		TOH (n-1, helper, end, start);
	}
}






// HELPER FUNCTIONS
// wrapper to simplify the code above 
void move (int start, int end)
{
	t.move(start, end);
}



class Tower
{
	private int n;
	private ArrayList<Ring> s,e,h;
	private int x1, x2, x3;
	public final int RWIDTH= 100;
	public final int RHEIGHT= 20;
	private int speed;

	Tower (int rings)
	{
		s= new ArrayList<Ring>();
		e= new ArrayList<Ring>();
		h= new ArrayList<Ring>();

		int wpiece= RWIDTH/rings;
		for (int i=0; i<rings;i++)
		{
			s.add(new Ring (wpiece*(rings-i), RHEIGHT));
		}

		int wgap= width/4;
		x1= wgap;
		x2= width/2;
		x3= wgap*3;

		speed= 1000;
	}

	void setSpeed(int ms)
	{
		speed= ms;
	}

	void draw()
	{
		// draw three poles
		stroke(0);
		strokeWeight(5);
		line(x1,width,x1,height/3);
		line(x2,width,x2,height/3);
		line(x3,width,x3,height/3);

		drawDisks (s, x1);
		drawDisks (h, x2);
		drawDisks (e, x3);
	}

	void move (ArrayList<Ring> from, ArrayList<Ring> to)
	{
		if (from.size()==0) return;
		to.add (from.remove(from.size()-1));
		try 
		{
			Thread.sleep(speed);
		}catch (Exception e){}
	}

	void move (int from, int to)
	{
		ArrayList<Ring> a;
		if (from==0) a=s;
		else if (from==1) a=h;
		else a=e;

		ArrayList<Ring> b;
		if (to==0) b=s;
		else if (to==1) b=h;
		else b=e;

		move(a,b);
	}

	void drawDisks (ArrayList<Ring> list, int location)
	{
		int y= height-RHEIGHT/2;
		for (Ring r: list)
		{
			r.draw(location, y);
			y-=RHEIGHT;
		}
	}


	class Ring
	{
		color c;
		int w, h;

		Ring (int width, int height)
		{
			c= color (int(random(0,256)),int(random(0,256)),int(random(0,256)));
			w= width; h= height;
		}

		void draw (int x, int y)
		{
			rectMode(CENTER);
			fill(c);
			noStroke();
			rect(x,y,w,h);
		}
	}
}