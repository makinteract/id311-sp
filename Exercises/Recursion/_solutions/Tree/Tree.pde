Turtle t;
int length= 100;

void setup() {
    size(800,600);    
    t = new Turtle();
    t.setxy(0,length*2);
    noLoop();
}
void draw() {
    background(255);
    stroke(0,255,0);
    tree (t, length);
}

void tree (Turtle t, int branchLen)
{
    if (branchLen > 5)
    {
        t.forward(branchLen);
        t.right(20);
        tree(t, branchLen-branchLen/4);
        t.left(40);
        tree(t, branchLen-branchLen/4);
        t.right(20);
        t.back(branchLen);
    }
}
