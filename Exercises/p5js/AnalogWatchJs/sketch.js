// Analog watch js

// 1. TODO craeting two objects

/*
	Watches are objects with the following properties
	Initialize the objects in TODO 2 below
	
	Properties:
		loc: a vector with the x y location
		size: the size of the radius
		bgColor: the color of the watch
		hour: default set to 0
		min: default set to 0
		sec: default set to 0
*/


function setup() 
{
	createCanvas(800, 600);

	// 2. TODO
	// initWatch (watch1, width/3, height/2, 100, 10);
	// initWatch (watch2, 2*width/3, height/2, 100, 100);
}

function draw()
{
	background(200);

	// 3. TODO
	//drawWatch (watch1);
	//drawWatch (watch2);
}

// TODO: function serves as constructor
function initWatch (w, x, y, size, refreshMs)
{
	
}

// All done below here: drawing function
function drawWatch(w)
{
	ellipseMode(CENTER);
	stroke(0);
	strokeWeight(w.size/10);
	fill(w.bgColor);
	ellipse(w.loc.x, w.loc.y, w.size*2, w.size*2);
	// drawLineWithAngle (w.loc.x, w.loc.y, 0, w.size);
	drawSec (w.loc.x, w.loc.y, w.sec, w.size)
	drawMin (w.loc.x, w.loc.y, w.min, w.size)
	drawHour (w.loc.x, w.loc.y, w.hour, w.size)

	// inner functions (hidden)
	function drawSec (x, y, sec, length)
	{
		const a= -PI/2 + 2*PI*sec/60.0;
		strokeWeight (length/50);
		stroke(255,0,0);
		drawLineWithAngle(x, y, a, length*4.0/5);
	}

	function drawMin (x, y, min, length)
	{
		const a= -PI/2 + 2*PI*min/60.0;
		strokeWeight (length/30);
		stroke(0);
		drawLineWithAngle(x, y, a, length*4.0/5);
	}

	function drawHour (x, y, hour, length)
	{
		if (hour>12) hour-=12;
		const a= -PI/2 + 2*PI*hour/12.0;
		strokeWeight (length/20);
		stroke(0);
		drawLineWithAngle(x, y, a, length*4.0/5);
	}

	function drawLineWithAngle (x, y, a, length)
	{
		translate(x, y);
		rotate(a);
		line(0,0,length,0);
		resetMatrix();
	}
}