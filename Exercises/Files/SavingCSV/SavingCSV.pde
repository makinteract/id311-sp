ArrayList<PVector> points;

void setup()
{
	size(800, 600);
	points= new ArrayList<PVector>();
	loadPoints("mypoints.csv");
}

void draw()
{
	background(0);
	fill(255);
	noStroke();
	for (PVector p: points)
		ellipse (p.x, p.y, 10, 10);
}

void exit()
{
	// called when exit program
	savePoints ("mypoints.csv");
	System.exit(0);
}


void mousePressed()
{
	points.add (new PVector(mouseX, mouseY));
}


	
void loadPoints (String filename)
{
	// If the file is not there, stop
	File f = new File(sketchPath(filename));
	if(!f.exists()) return;

	// continue
}


void savePoints (String filename)
{
	// write the points on file as x,y per line
}






