ArrayList<PVector> points;

void setup()
{
	size(800, 600);
	points= new ArrayList<PVector>();
	loadPoints("mypoints.json");
}

void draw()
{
	background(0);
	fill(255);
	noStroke();
	for (PVector p: points)
		ellipse (p.x, p.y, 10, 10);
}

void exit()
{
	// called when exit program
	savePoints ("mypoints.json");
}


void mousePressed()
{
	points.add (new PVector(mouseX, mouseY));
}


	
void loadPoints (String filename)
{
	// If file does not exists
	File f = new File(sketchPath(filename));
	if(!f.exists()) return;

	//...
	
}


void savePoints (String filename)
{
	JSONObject json = new JSONObject();
	//...

	saveJSONObject(json, filename);
}






