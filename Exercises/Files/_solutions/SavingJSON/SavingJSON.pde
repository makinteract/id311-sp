ArrayList<PVector> points;

void setup()
{
	size(800, 600);
	points= new ArrayList<PVector>();
	loadPoints("mypoints.json");
}

void draw()
{
	background(0);
	fill(255);
	noStroke();
	for (PVector p: points)
		ellipse (p.x, p.y, 10, 10);
}

void exit()
{
	// called when exit program
	savePoints ("mypoints.json");
}


void mousePressed()
{
	points.add (new PVector(mouseX, mouseY));
}


	
void loadPoints (String filename)
{
	// If file does not exists
	File f = new File(sketchPath(filename));
	if(!f.exists()) return;

	JSONObject json = loadJSONObject(filename);
	JSONArray pts = json.getJSONArray("points");
	for (int i = 0; i < pts.size(); i++) 
	{
    	JSONObject point= pts.getJSONObject(i); 
    	points.add (new PVector (point.getFloat("x"), point.getFloat("y")));
    }
	
}


void savePoints (String filename)
{
	JSONObject json = new JSONObject();
	JSONArray pts = new JSONArray();

	json.setJSONArray("points", pts);
	

	int index=0;
	for (PVector p: points)
	{
		JSONObject point = new JSONObject();
		point.setFloat("x", p.x);
		point.setFloat("y", p.y);
		pts.setJSONObject(index, point); 
		index++;
	}

	saveJSONObject(json, filename);
}






