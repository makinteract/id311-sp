ArrayList<PVector> points;

void setup()
{
	size(800, 600);
	points= new ArrayList<PVector>();
	loadPoints("mypoints.csv");
}

void draw()
{
	background(0);
	fill(255);
	noStroke();
	for (PVector p: points)
		ellipse (p.x, p.y, 10, 10);
}

void exit()
{
	// called when exit program
	savePoints ("mypoints.csv");
	System.exit(0);
}


void mousePressed()
{
	points.add (new PVector(mouseX, mouseY));
}


	
void loadPoints (String filename)
{
	// If the file is not there, stop
	File f = new File(sketchPath(filename));
	if(!f.exists()) return;

	// continue
	String[] str= loadStrings (filename);
	for (String s: str)
	{	
		String[] t= splitTokens(s, ",");
		if (t.length != 2) continue;
		points.add (new PVector (parseFloat(t[0]),parseFloat(t[1])));
	}

}


void savePoints (String filename)
{
	PrintWriter pw= createWriter(filename);
	for (PVector p: points)
	{
		pw.println(p.x +","+ p.y);
		pw.flush();
	}
	pw.close();
}






