class Ball
{
	private String output;

	Ball ()
	{
		output="I am a Ball...";
	}

	void setLeather()
	{
		output+= "made of leather";
	}

	void setRoundShape()
	{
		output+= "with round shape...";
	}

	void setOvalShape()
	{
		output+= "with oval shape...";
	}

	void setColor (String col)
	{
		output+= "colored "+col+"...";
	}

	void setPressure (float psi)
	{
		output+= "with pressure "+psi+"...";
	}

	void printStats()
	{
		println(output);
	}
}




class Factory
{
	Ball getSoccerBall()
	{
		Ball b= new Ball();
		b.setLeather();
		b.setRoundShape();
		b.setColor ("black & white");
		b.setPressure (8.7);
		return b;
	}

	Ball getFootball()
	{
		Ball b= new Ball();
		b.setLeather();
		b.setOvalShape();
		b.setColor ("brown");
		b.setPressure (8.7);
		return b;
	}
}




void setup()
{
	Factory f= new Factory();
	Ball b= f.getSoccerBall();
	b.printStats();

	b= f.getFootball();
	b.printStats();

}

