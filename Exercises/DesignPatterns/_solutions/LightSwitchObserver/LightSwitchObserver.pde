ArrayList<Light> lights= new ArrayList<Light>();
ArrayList<AirConditioner> acs= new ArrayList<AirConditioner>();
Switch s1, s2, s3;

void setup()
{	
	size (800, 600);

	lights.add (new Light(width/5, 90));
	lights.add (new Light(width/5*4, 90));
	acs.add( new AirConditioner (width/2, 300) );

	s1= new Switch (width/3, 50);
	s2= new Switch (width/2, 50);
	s3= new Switch (2*width/3, 50);

	s1.addObservable(lights.get(0));
	s2.addObservable(lights.get(1));
	s3.addObservable(acs.get(0));
	
}

void draw()
{
	background(255);
	for (Light l: lights) l.draw();
	for (AirConditioner ac: acs) ac.draw();
	s1.draw();
	s2.draw();
	s3.draw();
}

void mousePressed()
{
	s1.swithOnOff();
	s2.swithOnOff();
	s3.swithOnOff();
}


interface Powerable
{
	void switchOnOff();
}




class Light implements Powerable
{
	private int x, y;
	private int w, h;
	private PImage [] imgs;
	boolean onState;

	Light (int x, int width)
	{
		imgs= new PImage [2];
		imgs[0]= loadImage ("lightOff.jpg");
		imgs[1]= loadImage ("lightOn.jpg");
		w= width;
		float ratio= float(imgs[0].height)/imgs[0].width;
		h= int(w*ratio);

		this.x= x;
		this.y= h/2;
		onState= false;
	}	

	void switchOnOff (){ onState= !onState; }
	boolean isOn (){ return onState; }

	void draw()
	{
		imageMode(CENTER);
		if (isOn()) image(imgs[1], x, y, w, h);
		else image(imgs[0], x, y, w, h);
	}
}




class AirConditioner implements Powerable
{
	private boolean onState;
	private int x, y;
	private int w, h;
	private PImage [] imgs;

	AirConditioner (int x, int width)
	{
		imgs= new PImage [2];
		imgs[0]= loadImage ("acOff.png");
		imgs[1]= loadImage ("acOn.png");
		w= width;
		float ratio= float(imgs[0].height)/imgs[0].width;
		h= int(w*ratio);

		this.x= x;
		this.y= h/2;
		onState= false;
	}	

	void switchOnOff (){ onState= !onState; }
	boolean isOn (){ return onState; }

	void draw()
	{
		imageMode(CENTER);
		if (isOn()) image(imgs[1], x, y, w, h);
		else image(imgs[0], x, y, w, h);
	}
}





class Switch 
{
	private PImage imgs[];
	private boolean onState;
	private int x, y;
	private int w, h;
	private ArrayList<Powerable> observables;

	Switch (int x, int width)
	{
		imgs= new PImage [2];
		imgs[0]= loadImage ("switchOff.jpg");
		imgs[1]= loadImage ("switchOn.jpg");
		w= width;
		float ratio= float(imgs[0].height)/imgs[0].width;
		h= int(w*ratio);

		this.x= x;
		y= height-h/2;

		observables= new ArrayList<Powerable>();
	}

	void addObservable (Powerable obs)
	{
		observables.add (obs);
	}

	boolean isOn(){return onState; }

	void draw()
	{
		imageMode(CENTER);
		if (isOn()) image(imgs[1], x, y, w, h);
		else image(imgs[0], x, y, w, h);
	}

	boolean isOver()
	{
		if (mouseX < x-w/2 || mouseX > x+w/2) return false;
		if (mouseY < y-h/2 || mouseY > y+h/2) return false;
		return true;
	}

	void swithOnOff()
	{
		if (!isOver()) return;
		onState= !onState;
		for (Powerable o: observables)
			o.switchOnOff();
	}
}





