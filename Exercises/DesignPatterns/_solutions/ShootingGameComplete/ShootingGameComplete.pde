import processing.sound.*;
import java.util.Observer;
import java.util.Observable;


Gun gun;
ArrayList<Target> targets;
ScoreDisplay s;
Factory fac;


void setup() 
{
	size(800, 600);
	gun = new Gun();
	s = new ScoreDisplay();
	gun.addObserver (s);
	fac= new Factory();

	initTargets();
}


void draw() 
{
	background(255);
	for (Target t : targets)
		t.draw();
	gun.draw();
	s.draw();
}


void mousePressed() {
	PVector coord = gun.shoot();
	for (Target t : targets)
		t.shoot(coord);
}


void keyPressed() {
	initTargets();
	gun.reload();
}


void initTargets() {
	targets = new ArrayList<Target>();

	for (int i = 0; i < 5; i++) {
		Target t = fac.getRandomTarget(100 + i * width / 5, height / 2);
		if (t==null) continue;
		t.addObserver (s);
		targets.add (t);
	}
}




class ScoreDisplay implements Observer 
{
	ScoreDisplay () {
		img = loadImage("bullet.png");
		shotLeft = Gun.TOT_SHOTS;
		ScoreDisplay= 0;

		font= createFont("font.vlw", 32);
		textFont(font);
	}

	void draw() {
		for (int i = 0; i < shotLeft; i++) {
			image(img, width - BULLET_SIZE - BULLET_SIZE * i, BULLET_SIZE, BULLET_SIZE, BULLET_SIZE);
		}
		fill(255,0,0);
		text(ScoreDisplay, 12, 60);
	}

	public void update(Observable obs, Object obj) {
		
		if (obs instanceof Gun)
		{
			Gun g = (Gun)obs;
			if (g == null) return;
			shotLeft= g.getRemainingShots();

		}else if (obs instanceof Target)
		{
			Target t= (Target)obs;
			if (t==null) return;
			ScoreDisplay+= t.getPoints();
		}
	}

	PFont font;
	PImage img;
	int shotLeft;
	int ScoreDisplay;
	public final int BULLET_SIZE = 20;
}



class Gun extends Observable 
{
	Gun() {
		noCursor();
		curs = loadImage("cursor.png");
		shotSound = new SoundFile(ShootingGameComplete.this, "shot.mp3");
		emptySound = new SoundFile(ShootingGameComplete.this, "empty.mp3");
		bullets = new Bullet[TOT_SHOTS]; // max 50 on the screen
		reload();}

	void reload(){
		remainingShots=0;

		setChanged();
		notifyObservers();
	}

	int getRemainingShots() {
		return TOT_SHOTS-remainingShots;}

	void draw() {
		if (curs == null)  return;
		imageMode(CENTER);
		image (curs, mouseX, mouseY, CURSOR_SIZE, CURSOR_SIZE);

		for (int i = 0; i < bullets.length; i++)
			if (bullets[i] != null) bullets[i].draw(); 
	}


	PVector shoot() {
		if (remainingShots >= TOT_SHOTS) {
			emptySound.play();
			return null;
		}

		float x = mouseX + random(-CURSOR_SIZE / 2, CURSOR_SIZE / 2);
		float y = mouseY + random(-CURSOR_SIZE / 2, CURSOR_SIZE / 2);
		bullets[remainingShots++] = new Bullet (x, y);
		shotSound.play();

		setChanged();
		notifyObservers();

		return new PVector(x, y);
	}

	private PImage curs;
	SoundFile shotSound, emptySound;
	Bullet [] bullets;
	int remainingShots;
	public static final int TOT_SHOTS = 6;
	public final int CURSOR_SIZE = 80;
}




class Bullet extends Thread 
{
	Bullet(float x, float y) {
		super();
		img = loadImage("bulletHole.png");
		visible = true;
		this.x = x;
		this.y = y;
		start();
	}

	void draw() {
		if (!visible) return;
		if (img == null) return;
		imageMode(CENTER);
		image (img, x, y, SIZE, SIZE);
	}

	void run() {
		try {
			Thread.sleep (DURATION);
		} catch (Exception e) {}

		visible = false;
	}


	float x, y;
	PImage img;
	boolean visible;
	int time;

	public final int DURATION = 2000;
	public final int SIZE = 25;
}



