abstract class Target extends Observable
{
	abstract int getPoints();

	Target(int x, int y) {
		super();
		visible = true;
		pos = new PVector (x, y);
	}

	void draw() {
		if (!visible) return;
		image (img, pos.x, pos.y, WIDTH, height);
	}

	boolean isHit() {
		return !visible;
	}


	void shoot (PVector loc) 
	{
		if (!visible) return;
		if (loc==null) return;

		if (loc.x < pos.x - WIDTH / 2) visible = true;
		else if (loc.x > pos.x + WIDTH / 2) visible = true;
		else if (loc.y < pos.y - height / 2) visible = true;
		else if (loc.y > pos.y + height / 2) visible = true;
		else visible = false;

		if (!visible)
		{
			setChanged();
      		notifyObservers();
      	}
	}

	protected PImage img;
	protected float height;
	protected boolean visible;
	protected PVector pos;

	public final float WIDTH = 100;
	public final float SPEED = 2;
}


class TeddyTarget extends Target 
{
	TeddyTarget(int x, int y) {
		super(x, y);
		img = loadImage ("teddy.png");
		height = img.height * (WIDTH / img.width);
	}

	int getPoints() {
		if (isHit()) return 1;
		return 0;
	}
}


class DuckTarget extends Target {
	DuckTarget(int x, int y) {
		super(x, y);
		img = loadImage ("duck.png");
		height = img.height * (WIDTH / img.width);
	}

	int getPoints() {
		if (isHit()) return 3;
		return 0;
	}
}

class SquirrelTarget extends Target {
	SquirrelTarget(int x, int y) {
		super(x, y);
		img = loadImage ("squirrel.png");
		height = img.height * (WIDTH / img.width);
	}

	int getPoints() {
		if (isHit()) return 5;
		return 0;
	}
}


class Factory {
	
	Target getTarget (String name, int x, int y) {
		if (name.equals("teddy")) return new TeddyTarget (x, y);
		else if (name.equals("duck")) return new DuckTarget (x, y);
		else if (name.equals("squirrel")) return new SquirrelTarget (x, y);
		return null;
	}

	Target getRandomTarget(int x, int y) {
		String [] options = {"teddy", "duck", "squirrel"};
		return getTarget (options[(int)(random(3))], x, y);
	}
}