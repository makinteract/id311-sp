import processing.sound.*;
import java.util.Observer;
import java.util.Observable;


Gun gun;
ArrayList<Target> targets;
ScoreDisplay s;
Factory fac;


void setup() 
{
	size(800, 600);
	gun = new Gun();
	s = new ScoreDisplay();
	fac= new Factory();

	initTargets();
}


void draw() 
{
	background(255);
	for (Target t : targets)
		t.draw();
	gun.draw();
	s.draw();
}


void mousePressed() {
	PVector coord = gun.shoot();
	for (Target t : targets)
		t.shoot(coord);
}


void keyPressed() {
	initTargets();
	gun.reload();
}


void initTargets() {
	targets = new ArrayList<Target>();

	for (int i = 0; i < 5; i++) {
		Target t = fac.getRandomTarget(100 + i * width / 5, height / 2);
		if (t==null) continue;
		t.addObserver (s);
	}
}












