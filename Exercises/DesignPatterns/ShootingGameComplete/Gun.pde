class Gun extends Observable 
{
	Gun() {
		noCursor();
		curs = loadImage("cursor.png");
		shotSound = new SoundFile(ShootingGameComplete.this, "shot.mp3");
		emptySound = new SoundFile(ShootingGameComplete.this, "empty.mp3");
		bullets = new Bullet[TOT_SHOTS]; // max 50 on the screen
		reload();}

	void reload(){
		remainingShots=0;

		setChanged();
		notifyObservers();
	}

	int getRemainingShots() {
		return TOT_SHOTS-remainingShots;}

	void draw() {
		if (curs == null)  return;
		imageMode(CENTER);
		image (curs, mouseX, mouseY, CURSOR_SIZE, CURSOR_SIZE);

		for (int i = 0; i < bullets.length; i++)
			if (bullets[i] != null) bullets[i].draw(); 
	}


	PVector shoot() {
		if (remainingShots >= TOT_SHOTS) {
			emptySound.play();
			return null;
		}

		float x = mouseX + random(-CURSOR_SIZE / 2, CURSOR_SIZE / 2);
		float y = mouseY + random(-CURSOR_SIZE / 2, CURSOR_SIZE / 2);
		bullets[remainingShots++] = new Bullet (x, y);
		shotSound.play();

		setChanged();
		notifyObservers();

		return new PVector(x, y);
	}

	private PImage curs;
	SoundFile shotSound, emptySound;
	Bullet [] bullets;
	int remainingShots;
	public static final int TOT_SHOTS = 6;
	public final int CURSOR_SIZE = 80;
}




class Bullet extends Thread 
{
	Bullet(float x, float y) {
		super();
		img = loadImage("bulletHole.png");
		visible = true;
		this.x = x;
		this.y = y;
		start();
	}

	void draw() {
		if (!visible) return;
		if (img == null) return;
		imageMode(CENTER);
		image (img, x, y, SIZE, SIZE);
	}

	void run() {
		try {
			Thread.sleep (DURATION);
		} catch (Exception e) {}

		visible = false;
	}


	float x, y;
	PImage img;
	boolean visible;
	int time;

	public final int DURATION = 2000;
	public final int SIZE = 25;
}