class ScoreDisplay implements Observer 
{
	ScoreDisplay () {
		img = loadImage("bullet.png");
		shotLeft = Gun.TOT_SHOTS;
		ScoreDisplay= 0;

		font= createFont("font.vlw", 32);
		textFont(font);
	}

	void draw() {
		for (int i = 0; i < shotLeft; i++) {
			image(img, width - BULLET_SIZE - BULLET_SIZE * i, BULLET_SIZE, BULLET_SIZE, BULLET_SIZE);
		}
		fill(255,0,0);
		text(ScoreDisplay, 12, 60);
	}

	public void update(Observable obs, Object obj) {
		
		if (obs instanceof Gun)
		{
			Gun g = (Gun)obs;
			if (g == null) return;
			shotLeft= g.getRemainingShots();

		}else if (obs instanceof Target)
		{
			Target t= (Target)obs;
			if (t==null) return;
			ScoreDisplay+= t.getPoints();
		}
	}

	PFont font;
	PImage img;
	int shotLeft;
	int ScoreDisplay;
	public final int BULLET_SIZE = 20;
}