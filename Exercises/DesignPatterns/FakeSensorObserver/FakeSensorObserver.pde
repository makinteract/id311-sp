GeoDisplay display;
GpsSensor sensor;

void setup()
{
  size(800, 600);

  display= new GeoDisplay();
  sensor= new GpsSensor ();
  
  (new Thread(sensor)).start();
}


void draw()
{
  background(100);
  display.draw();
}



class GeoLocation
{
  private float lat, lon, altitude;

  GeoLocation (float lat, float lon, float altitude)
  {
    this.lat= lat;
    this.lon= lon;
    this.altitude= altitude;
  }

  String toString()
  {
    return "Latitude: "+ lat+
         "\nLongitude: "+lon+
         "\nAltitude: "+altitude;  
  }
}




interface GpsEvent
{
  void onLocationChange(GeoLocation gl);
}


// Observable / Listenable
class GpsSensor implements Runnable
{
  public final int MIN_REFRESH_RATE = 2000;
  public final int MAX_REFRESH_RATE = 20000;


  GpsSensor()
  {
  }

  void run()
  {
    while (true)
    {  
      try {
        Thread.sleep(MIN_REFRESH_RATE, MAX_REFRESH_RATE);
      }catch (Exception e) {}
    
      float lat= random(-90, 90);
      float lon= random(-180, 180);
      float alti= random (0, 8848);
      GeoLocation gl= new GeoLocation(lat, lon, alti);
      println(gl);
    }
  }
}

// Observer / Listener
class GeoDisplay
{
  private GeoLocation location;
  private PFont font;


  GeoDisplay()
  {
    location= new GeoLocation(0,0,0);
    font = loadFont("font.vlw");
  }

  void draw()
  {
    fill(255);
    textFont(font);
    textAlign(CENTER, CENTER);
    text(location.toString(), width/2, height/2);
  }
}