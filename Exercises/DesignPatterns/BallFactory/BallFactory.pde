class Ball
{
	private String output;

	Ball ()
	{
		output="I am a Ball...";
	}

	void setLeather()
	{
		output+= "made of leather";
	}

	void setRoundShape()
	{
		output+= "with round shape...";
	}

	void setOvalShape()
	{
		output+= "with oval shape...";
	}

	void setColor (String col)
	{
		output+= "colored "+col+"...";
	}

	void setPressure (float psi)
	{
		output+= "with pressure "+psi+"...";
	}

	void printStats()
	{
		println(output);
	}
}





void setup()
{
	// Manual instantiation
	// Can we do better?
	Ball b= new Ball();
	b.setRoundShape();
	b.setLeather();
	b.setColor("B & w");
	b.setPressure (800);
	b.printStats();

}


















