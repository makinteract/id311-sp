final int IMG_SIZE = 200;

ArrayList <Vehicle> vehicles;

String message ="";
PFont font;


void setup()
{
	size (800, 300);
	font = loadFont("font.vlw");

	vehicles= new ArrayList <Vehicle> ();

	vehicles.add (new Car(4)); // upcasting
	vehicles.add (new SportCar (true)); // upcasting
	vehicles.add (new Convertible()); // upcasting
	vehicles.add (new Truck(1000)); // upcasting
}

void draw()
{
	background(255);

	for (int i=0; i<vehicles.size(); i++)
	{
		Vehicle v= vehicles.get(i);
		image (v.getImage(), i*IMG_SIZE, 0, IMG_SIZE, IMG_SIZE);
	}

	fill (0);
	textFont(font, 20);
	textAlign(CENTER);
	text(message, width/2, height-30);

}


void mousePressed()
{
	int index= mouseX / IMG_SIZE;
	Vehicle v= vehicles.get(index);

	if (index == 0)
	{
		Car c= (Car)v;  // downcasting
		message= c.drive();
		
	}else if (index == 1)
	{
		SportCar sc= (SportCar)v;  // downcasting
		message= sc.drive() + sc.turbo() + sc.option();

	}else if (index == 2)
	{
		Convertible c= (Convertible)v;  // downcasting
		message= c.drive() + c.turbo() + c.option() + c.openRooftop();

	}else if (index == 3)
	{
		Truck t= (Truck)v;  // downcasting
		message= t.drive();
	}
}



abstract class Vehicle
{
	protected PImage img;

	// Vehicle() {}   // default constructor (no need)

	PImage getImage(){
		return img;
	}

	abstract String drive(); // must override
}




class Car extends Vehicle
{
	private int passengers;

	Car (int passengers)
	{
		super();
		this.passengers= passengers;
		img= loadImage("car.jpg");
	}

	@Override
	String drive()
	{
		return "I drive a car with "+ passengers +" passengers. ";
	}

}




class SportCar extends Car
{
	private boolean fullOp;


	SportCar(boolean fullOption)
	{
		super(2);
		fullOp= fullOption;
		img= loadImage("sport.jpg");
	}

	String turbo()
	{
		return "I have a turbo. ";
	}

	String option()
	{
		if (fullOp) return "I am full-option. ";
		return "";

	}
}

class Convertible extends SportCar
{
	Convertible ()
	{
		super (true);
		img= loadImage("convertible.jpg");
	}

	String openRooftop()
	{
		return "I have a rooftop. ";
	}
}



class Truck extends Vehicle
{
	private int load;


	Truck (int load)
	{
		this.load= load;
		img= loadImage("truck.jpg");
	}

	@Override
	String drive()
	{
		return "I drive a truck with "+ load +" Kg. ";
	}

}












