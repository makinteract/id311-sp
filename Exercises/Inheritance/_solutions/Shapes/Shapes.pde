// Some constants for you
public final color RED= color(255,0,0);
public final color BLUE= color(0,0,255);

// Globals
ArrayList<Shape> shapes= new ArrayList<Shape>();

void setup()
{
	size(800,600);
	shapes.add (new Rectangle(width/4, height/2, 150, 50, RED));
	shapes.add (new Circle(width/2, height/2, 150, RED));
	shapes.add (new Square(3*width/4, height/2, 150, RED));
}

void draw()
{
	background(255);

	// We'll do in a better way next week
	// for the moment, bare with me!
	for (Shape s: shapes)
	{
		if (s instanceof Rectangle)
		{
			Rectangle r= (Rectangle)s;
			r.drawRect();
		}else if (s instanceof Circle)
		{
			Circle c= (Circle)s;
			c.drawCircle();
		}if (s instanceof Square)
		{
			Square q= (Square)s;
			q.drawSquare();
		}
	}

}


abstract class Shape
{
	protected int x, y;
	protected color c;

	Shape (int x, int y, color c)
	{
		this.x=x; 
		this.y=y; 
		this.c=c;
	}

	void draw()
	{
		if (isMouseOver()) fill(c);
		else fill(BLUE);
	}

	abstract boolean isMouseOver();
}

class Rectangle extends Shape
{	
	protected int w, h;

	Rectangle (int x, int y, int w, int h, color c)
	{
		super(x,y,c);
		this.w= w;
		this.h= h;
	}

	void drawRect()
	{
		super.draw();
		rectMode(CENTER);
		rect(x,y,w,h);
	}


	@Override
	boolean isMouseOver()
	{
		if (mouseX < x-w/2 || mouseX > x+w/2) return false;
		if (mouseY < y-h/2 || mouseY > y+h/2) return false;
		return true;
	}
}



class Circle extends Shape
{	
	private int diameter;

	Circle (int x, int y, int diameter, color c)
	{
		super(x,y,c);
		this.diameter= diameter;
	}

	void drawCircle()
	{
		super.draw();
		ellipseMode(CENTER);		
		ellipse(x,y,diameter,diameter);
	}

	@Override
	boolean isMouseOver()
	{
		int dx= mouseX-x;
		int dy= mouseY-y;
		return sqrt(dx*dx + dy*dy) < diameter/2;
	}
}



class Square extends Rectangle
{

	Square (int x, int y, int size, color c)
	{
		super(x,y,size,size,c);
	}

	void drawSquare()
	{
		super.drawRect();
	}
}

	