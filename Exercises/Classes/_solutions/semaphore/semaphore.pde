Semaphore s;

void setup()
{
	size (800, 600);
	s= new Semaphore (width/2, height/2, 100);
}

void draw()
{
 	background(255);
 	s.draw();
}

int counter= 0;
void mousePressed()
{
	switch (counter) {
		case 0: s.setRed(); break;
		case 1: s.setYellow(); break;
		case 2: s.setGreen(); break;
	}
	counter= (counter+1) % 3;
}


enum SemaphoreState { RED, GREEN, YELLOW };

class Semaphore
{
	private int x, y, diam;
	private SemaphoreState current;
	public final int LOW_CONTRANS = 50;
	public final int HIGH_CONTRAST = 255;


	public Semaphore (int x, int y, int diam)
	{
		this.x= x; this.y= y; this.diam= diam;
		current= SemaphoreState.RED;
	}

	public void setState (SemaphoreState s){ current= s; }
	public SemaphoreState getState(){ return current; }

	public void setGreen(){ current= SemaphoreState.GREEN; }
	public void setYellow(){ current= SemaphoreState.YELLOW; }
	public void setRed(){ current= SemaphoreState.RED; }



	public void draw()
	{
		stroke(0);
		strokeWeight(2);
		ellipseMode(CENTER);
		// red
		if (current == SemaphoreState.RED) fill (255, 0, 0, HIGH_CONTRAST);
		else fill (255, 0, 0, LOW_CONTRANS);
		ellipse (x, y-diam, diam, diam);
		// yellow
		if (current == SemaphoreState.YELLOW) fill (255, 255, 0, HIGH_CONTRAST);
		else fill (255, 255, 0, LOW_CONTRANS);
		ellipse (x, y, diam, diam);
		// green
		if (current == SemaphoreState.GREEN) fill (0, 255, 0, HIGH_CONTRAST);
		else fill (0, 255, 0, LOW_CONTRANS);;
		ellipse (x, y+diam, diam, diam);
	}
}










