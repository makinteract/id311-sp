import java.util.Collections;


abstract class Animal 
{
	abstract void makeSound();
}

abstract class Mammal extends Animal
{
	//void makeSound();
}

class Dog extends Mammal
{
	void makeSound() { println ("WOF WOF"); }
}

class Cat extends Mammal
{
	void makeSound() { println ("MIAO MIAO"); }
}


ArrayList <Animal> animals;


void setup()
{
	animals= new ArrayList <Animal>();
	animals.add (new Cat());
	animals.add (new Dog());
	animals.add (new Cat());
	animals.add (new Dog());
	animals.add (new Cat());
	animals.add (new Dog());
	animals.add (new Cat());
	animals.add (new Dog());
	animals.add (new Dog());

	Collections.shuffle (animals);


	for (Animal a: animals)
	{
		a.makeSound();
	}
}




















