abstract class Button
{
	protected int x, y;
	protected color col;
	protected String id;

	Button (String id, int x, int y)
	{
		this.x= x; this.y= y; this.id= id;
		col= color (255,0,0);
	}

	String getID(){ return id; }


	abstract void draw();

	abstract boolean isPressed();
}

class RectButton extends Button
{
	private int w, h;

	RectButton (String id, int x, int y, int w, int h)
	{
		super (id, x, y);
		this.w= w; this.h= h;
	}

	@Override
	boolean isPressed()
	{
		if (mouseX < x-w/2 || mouseX > x+w/2) return false;
		if (mouseY < y-h/2 || mouseY > y+h/2) return false;
		return true;
	}

	@Override
	void draw()
	{
		rectMode(CENTER);
		fill(col);
		stroke(0);
		rect (x,y,w,h);
	}
}



class CircleButton extends Button
{
	private int r;

	CircleButton (String id, int x, int y, int r)
	{
		super (id, x, y);
		this.r= r;
		col= color (0,255,0);
	}

	@Override
	boolean isPressed()
	{
		int dx= mouseX-x;
		int dy= mouseY-y;
		return sqrt(dx*dx + dy*dy) < r;
	}

	@Override
	void draw()
	{
		ellipseMode(CENTER);
		fill(col);
		stroke(0);
		ellipse (x,y,r*2,r*2);
	}
}







ArrayList<Button> buttons;

void setup()
{
	size(800, 600);
	buttons= new ArrayList<Button> ();

	buttons.add( new RectButton("Button 1", width/3, height/2-100, 70, 30));
	buttons.add(  new CircleButton("Button 2", 2*width/3, height/2-100, 30));
	buttons.add( new RectButton("Button 3", width/3, height/2, 70, 30));
	buttons.add(  new CircleButton("Button 4", 2*width/3, height/2, 30));
	buttons.add( new RectButton("Button 5", width/3, height/2+100, 70, 30));
	buttons.add(  new CircleButton("Button 6", 2*width/3, height/2+100, 30));
}

void draw()
{
	background(255);
	for (Button b: buttons)
	{
		b.draw();
	}
}




void mousePressed()
{
	for (Button b: buttons)
	{
		if (b.isPressed()) println(b.getID());
		
	}
}







