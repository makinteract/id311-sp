// Template WebSocket client

let ballSize = 50;

function setup() {
  createCanvas(800, 600);

  // Example zoom buttons
  const buttonSize= 70;

  buttonBuilder (0, height-buttonSize, buttonSize, "zoom_in", "green", "#fff", function()
  {
    ballSize+= 10;
    if (ballSize> 400) ballSize=400;
  });

  buttonBuilder (width-buttonSize, height-buttonSize, buttonSize, "zoom_out", "white", "green", function()
  {
    ballSize-= 10;
    if (ballSize< 50) ballSize=50;
  });


  // Example menu button
  const icons = ['home', 'label', 'payment', 'note_add', 'not_started', 'face', 'check_circle', 'get_app']
  
  icons.forEach ( function (value, index) {
    buttonBuilder (index*(buttonSize+1), 0, buttonSize, value, "#000", "#fff", function()
    {
      console.log(`Pressing button ${index}`);
    });
  });
}


function draw(){
  background(100);

  ellipseMode(CENTER);
  fill(0,255,0);
  stroke(255);
  strokeWeight(ballSize/10);
  ellipse(width/2, height/2, ballSize, ballSize);
}




/* buttonBuilder

  Use this function to create simple buttons (factory)
  x, y = top left corner of button
  size = width and height of button
  iconImage = use Material icons from here: https://material.io/resources/icons/?style=baseline
  fg = foreground color "#000000", default "black"
  bg = background color "#000000", default "white"
  eventFn = the callback function
*/

function buttonBuilder (x, y, size, iconImage, fg="#000", bg="#fff", eventFn)
{
  const but= document.createElement("BUTTON");
  but.classList.add("button_icon");
  but.setAttribute("id", "buildButton");
  const icon = document.createElement(`i`);
  icon.innerHTML= iconImage.toLowerCase();
  icon.classList.add("material-icons");
  but.appendChild(icon);

  but.style.top = `${y}px`;
  but.style.left = `${x}px`;
  but.style.width = `${size}px`;
  but.style.height = `${size}px`;  
  but.style.backgroundColor = bg;
  but.style.color = fg;

  icon.style.fontSize = `${2*size/3}px`;
  but.onclick = eventFn;
  document.body.appendChild(but);

  return but;
}