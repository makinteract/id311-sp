/*
{
	"shape": "rect",
	"x": 100,
	"y": 200,
	"w": 50,
	"h": 40,
	"r": 100,	
	"g": 120,	
	"b": 200
}

{
	"shape": "circle",
	"x": 100,
	"y": 200,
	"diam": 50,
	"r": 0,	
	"g": 120,	
	"b": 200
}
*/


/*
	Websocket address: ws://localhost:8025/shapeInfo
	To test your code, feel free to use either
		1) WSClient
		2) Smart Websocket Client extension for Chrome (https://chrome.google.com/webstore/detail/smart-websocket-client/omalebghpgejjiaoknljcfmglgbpocdp?hl=en)

*/

import websockets.*;
import java.util.Iterator;

WebsocketServer ws;
ArrayList<Shape> shapes;
ShapeFactory sf;

void setup() {
	size(800, 600);
	shapes = new ArrayList<Shape>();
	sf= new ShapeFactory();

	ws = new WebsocketServer(this, 8025, "/shapeInfo");
}

void draw() {
	background(50, 109, 168);

	synchronized (shapes) {
		for (Shape s: shapes) if (s!=null) s.draw();
	}


}

void webSocketServerEvent(String msg) 
{
	JSONObject json= parseJSONObject(msg);
	try
	{
		synchronized (shapes) {
			if (json!=null)	shapes.add (sf.getShape(json));
		}
		
	}catch (InvalidShapeException ie)
	{
		println("Invalid command");
	}
}


class ShapeFactory
{
	ShapeFactory(){}

	Shape getShape (JSONObject obj) throws InvalidShapeException
	{
		if (obj==null) throw new InvalidShapeException();
		String shapeType= obj.getString("shape");
		int x= obj.getInt("x");
		int y= obj.getInt("y");
		int r= obj.getInt("r");
		int g= obj.getInt("g");
		int b= obj.getInt("b");

		if (shapeType.equals("rect"))
		{
			int w= obj.getInt("w");
			int h= obj.getInt("h");
			return new Rect(x,y,w,h,color(r,g,b));

		}else if (shapeType.equals("circle"))
		{
			int diam= obj.getInt("diam");
			return new Circle(x,y,diam,color(r,g,b));
		}
		
		// others
		return null;

	}
}




class InvalidShapeException extends Exception{}


abstract class Shape
{
	protected color c;
	protected int x, y;

	Shape (int x, int y, color c)
	{
		this.x= x; this.y= y; this.c=c;
	}

	abstract void draw();
}

class Rect extends Shape
{
	private int w, h;

	Rect (int x, int y, int w, int h, color c)
	{
		super (x,y,c);
		this.w= w; this.h= h;
	}

	void draw()
	{
		noStroke();
		fill(c);
		rectMode(CENTER);
		rect (x, y, w, h);
	}
}

class Circle extends Shape
{
	private int diam;

	Circle (int x, int y, int d, color c)
	{
		super (x,y,c);
		this.diam= d;
	}

	void draw()
	{
		noStroke();
		fill(c);
		ellipseMode(CENTER);
		ellipse (x, y, diam, diam);
	}
}





