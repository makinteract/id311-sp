// Template WebSocket client

function setup() {
  createCanvas(800, 600);
}

function draw() {
  background(0);
}

function mousePressed() {
    let command = {
        shape: "circle",
        x: mouseX,
        y: mouseY,
        diam: 50,
        r: 255,	
        g: 0,	
        b: 0
    };

  console.log(JSON.stringify(command));
  ws.send(JSON.stringify(command));
}

// called when loading the page
$(function () {
  ws = new WebSocket("ws://localhost:8025/shapeInfo");

  ws.onopen = function () {
    // Web Socket is connected, send data using send()
    console.log("Ready...");
  };

  ws.onmessage = function (evt) {
    var received_msg = evt.data;
    console.log("Message is received..." + received_msg);
  };

  ws.onclose = function () {
    // websocket is closed.
    console.log("Connection is closed...");
  };
});
