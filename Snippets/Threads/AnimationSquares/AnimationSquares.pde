ArrayList<Animation> ani;

void setup()
{
	size(800, 600);
	ani= new ArrayList<Animation>();
	for (int i=0; i<5; i++)
		ani.add (new Animation(random(width), random(height), random(50,200), random(5), (long)random(1000,5000)));
}

void draw()
{
	background(255);
	for (Animation a: ani) a.draw();
}



class Animation extends Thread
{
	float x, y, sz, scalingFactor, scaling;
	long startTime, msDuration;
	boolean increase;
	color c;

	Animation (float x, float y, float sz, float scalingFactor, long msDuration)
	{
		this.x= x; this.y= y; this.sz=sz; this.scalingFactor= scalingFactor;
		this.msDuration= msDuration;
		this.scaling= 1;
		increase= true;
		c= color (random(255),random(255),random(255));
		start();
	}

	void draw()
	{
		rectMode(CENTER);
		fill(c);
		noStroke();
		pushMatrix();
		translate(x, y);
		scale(scaling);
		rect(0,0,sz,sz,sz/4);
		popMatrix();
	}

	public void run()
	{
		startTime= millis();
		while (true)
		{
			long currentTime= millis()-startTime;
			if (currentTime > msDuration){ increase= !increase; startTime= millis();}
			float perc= (float)currentTime/msDuration;
			if (!increase) perc= 1-perc;
			scaling= 1+easeLinear(perc)*(scalingFactor-1);
		}
	}

	float easeLinear(float t)
	{
		return t;
	}

	float easeQuadraticInOut(float t)
	{
		float sqt= t*t;
		return sqt / (2.0f * (sqt-t) + 1.0f);
	}

	float easeCubicInOut(float t)
	{
		float c= t*t*t;
		float sqt= t*t;
		return c/(3*sqt-3*t+1);
	}
}


