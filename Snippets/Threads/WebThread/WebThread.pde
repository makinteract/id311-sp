import java.net.URL;
import java.io.*;


int x;

void setup()
{
	size(800,600);
}


void draw()
{
	background(0);
	fill(255,0,0);
	ellipse(x++,height/2, 50,50);
}


void mousePressed()
{
	// Version 1: No threads
	// printWebsiteAsString ("http://makinteract.kaist.ac.kr");

    // Version 2: Java threads
    WebParser parser= new WebParser ("http://makinteract.kaist.ac.kr");

    // Version 3: Processing threads
    // thread ("printWebsiteAsString");
}


// wrapper
void printWebsiteAsString()
{
    printWebsiteAsString ("http://makinteract.kaist.ac.kr");
}


void printWebsiteAsString (String websiteUrl)
{
    URL url;
    InputStream is = null;
    BufferedReader br;
    String line, result="";

    try {
        url = new URL(websiteUrl);
        is = url.openStream(); 
        br = new BufferedReader(new InputStreamReader(is));

        while ((line = br.readLine()) != null) {
            println (line);
        }
    } catch (Exception mue) {
         mue.printStackTrace();
    
    } finally {
        try {
            if (is != null) is.close();
        } catch (IOException ioe) {}
    }
}



class WebParser extends Thread
{
    String websiteUrl;

    WebParser (String myURL)
    {
        websiteUrl= myURL;
        start();
    }


    void run ()
    {
    	URL url;
    	InputStream is = null;
        BufferedReader br;
        String line, result="";

        try {
            url = new URL(websiteUrl);
            is = url.openStream(); 
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                println (line);
            }
        } catch (Exception mue) {
             mue.printStackTrace();
        
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {}
        }
    }
}


