import java.util.*;

void setup()
{
	println(upperCase("andrea_bianchi"));
	println(upperCaseRec("andrea_bianchi"));
}


String upperCase (String str)
{
	String result = "";
	for (int i=0; i<str.length(); i++)
	{
		char c= str.charAt(i);
		if (c>='a' && c<='z') c-= 'a'-'A';
		result+=c;
	}
	return result;
}



String upperCaseRec (String str)
{
	if (str.isEmpty()) return "";

	char c= str.charAt(0);
	if (c>='a' && c<='z') c-= 'a'-'A';
	return c + upperCaseRec (str.substring(1));
}























