import websockets.*;
import java.net.*;
import controlP5.*;

WebsocketClient wsc;
ControlP5 cp5;


int x, y, r, g, b;
final int SIZE = 50;


void setup() {
  size(800, 160);

  // GUI
  PFont font = createFont("arial",20);


  cp5 = new ControlP5(this);

  cp5.addTextfield("input")
     .setLabel ("WebSocket address here")
     .setPosition(20,20)
     .setSize(500,40)
     .setFont(font)
     .setFocus(true)
     .setColor(color(255,0,0))
     .setText("ws://localhost:8025/test")
     ;


  cp5.addButton("connect")
    .setLabel ("Connect")
    .setSize(150,40)
    .setFont(font)
    .setPosition(600, 20);

  cp5.addButton("test")
    .setLabel ("Send test")
    .setSize(150,40)
    .setFont(font)
    .setPosition(600, 80);
}


void draw() {
  background(120);
}


// GUI control


void connect (int theValue) {

  String url = cp5.get(Textfield.class,"input").getText();

  try {
    wsc = new WebsocketClient(this, url);
  }catch (Exception e)
  {
    println("=====  Connection refused ========");
    println(e.getMessage());
  }
}

void test (int theValue) {
  try {
    wsc.sendMessage("{\"message\":\"test\"}"); // convert to string
  } catch (Exception e) {
    println("=====  Connection refused ========");
    println(e.getMessage());
  }
}


public void input(String theText) {
  // automatically receives results from controller input
  println("a textfield event for controller 'input' : "+theText);
}
  // JSONObject json = new JSONObject();
  // json.setString("shape", shapeType);
  // json.setInt("x", x);
  // json.setInt("y", y);
  // json.setInt("r", r);
  // json.setInt("g", g);
  // json.setInt("b", b);
  // if (shapeType.equals("rect")) {
  //   json.setInt("w", SIZE);
  //   json.setInt("h", SIZE);
  // } else {
  //   json.setInt("diam", SIZE);
  // }

  // try {
  //   wsc.sendMessage(json.toString()); // convert to string
  // } catch (Exception e) {
  //   println("Connection refused");
  // }
// }
