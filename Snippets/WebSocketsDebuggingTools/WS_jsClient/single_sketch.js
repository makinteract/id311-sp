// Template WebSocket client

function setup() {
  createCanvas(800, 600);
}

function draw() {
  background(0);
}

function mousePressed() {
  ws.send(`{"mouseX":"${mouseX}"}`);
}

// called when loading the page
$(function () {
  ws = new WebSocket("ws://localhost:8025/test");

  ws.onopen = function () {
    // Web Socket is connected, send data using send()
    console.log("Ready...");
  };

  ws.onmessage = function (evt) {
    var received_msg = evt.data;
    console.log("Message is received..." + received_msg);
  };

  ws.onclose = function () {
    // websocket is closed.
    console.log("Connection is closed...");
  };
});
