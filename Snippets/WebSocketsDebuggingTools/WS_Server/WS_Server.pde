/*
	Websocket address: ws://localhost:8025/test
	To test your code, feel free to use either
		1) WSClient
		2) Smart Websocket Client extension for Chrome (https://chrome.google.com/webstore/detail/smart-websocket-client/omalebghpgejjiaoknljcfmglgbpocdp?hl=en)

*/

import websockets.*;
import java.util.Iterator;

WebsocketServer ws;

void setup() {
	size(200, 200);

	ws = new WebsocketServer(this, 8025, "/test");
}

void draw() {
	background(0,255,0);
}

void webSocketServerEvent(String msg) 
{
	JSONObject json= parseJSONObject(msg);
	try
	{
		if (json!=null)	
			{
				println("Received:");
				println(json);
				println("================");
			}
		
	}catch (Exception ie)
	{
		println("Exception: Invalid command");
	}
}

