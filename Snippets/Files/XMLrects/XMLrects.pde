ArrayList<Rect> rectangles;

void setup()
{
	size(800, 600);
	rectangles= new ArrayList<Rect>();
	loadXmlData ("data/rectangles.xml");
}

void draw()
{
	background(120);
	for (Rect r: rectangles) r.draw();
}


void loadXmlData(String filename)
{
	XML xml= loadXML(filename);
	XML[] children = xml.getChildren("Rect");

	for (int i = 0; i < children.length; i++) {
		XML coord= children[i].getChild("Coord");
		XML props= children[i].getChild("Props");

		int x = coord.getInt("x");
	    int y = coord.getInt("y");
	    int w = coord.getInt("width");
	    int h = coord.getInt("height");
	    
	    boolean centerMode= props.getString("origin").equals("center");
	    String col= props.getString("color");
	    color c= 0;
	    if (col.equals("red")) c=color(255,0,0);
	    else if (col.equals("blue")) c=color(0,0,255);
	    else if (col.equals("green")) c=color(0,255,0);

	    rectangles.add (new Rect (x,y,w,h,c,centerMode));
	}
}



class Rect
{
	private int x, y, w, h;
	color c;
	boolean centered;

	Rect (int x, int y, int w, int h, color c, boolean centered)
	{
		this.x=x; this.y=y; this.w=w; this.h=h;
		this.c=c; this.centered=centered;
	}

	void draw()
	{
		if (centered) rectMode(CENTER);
		else rectMode(CORNER);
		fill(c);
		noStroke();
		rect(x,y,w,h);
	}
}