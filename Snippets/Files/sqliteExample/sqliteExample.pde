import org.ibex.nestedvm.*;
import org.ibex.nestedvm.util.*;
import org.sqlite.*;
import java.sql.*;



void setup()
{
  Connection c = null;
  Statement stmt = null;
   try {
     Class.forName("org.sqlite.JDBC");
     // We need the full path of the database
     c = DriverManager.getConnection("jdbc:sqlite:"+sketchPath("data.db"));

     stmt = c.createStatement();
     ResultSet rs = stmt.executeQuery( "SELECT * FROM PEOPLE;" );
     while ( rs.next() ) {
        String name = rs.getString("NAME");
        int age = rs.getInt("AGE");
        System.out.println(name+": "+age);
     }
     rs.close();
     stmt.close();
     c.close();

   } catch ( Exception e ) {
     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
     System.exit(0);
   }
}
