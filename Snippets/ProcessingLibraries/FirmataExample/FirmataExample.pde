import processing.serial.*;

import cc.arduino.*;

Arduino arduino;
int pin = 10;

void setup() {
  
  println(Arduino.list());
  

  arduino = new Arduino(this, Arduino.list()[3], 57600);
  arduino.pinMode(pin, Arduino.OUTPUT);
}

void draw() {
  background(0);
}

void mousePressed()
{
  arduino.digitalWrite(pin, Arduino.HIGH);
}

void mouseReleased()
{
  arduino.digitalWrite(pin, Arduino.LOW);
}