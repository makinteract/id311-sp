import oscP5.*;
import netP5.*;

OscP5 oscP5;
String MULTICAST_ADDRESS= "127.0.0.1"; //239.0.0.1";
int OSC_PORT= 5204;


void setup() 
{
  size(100, 100);
  oscP5 = new OscP5(this, MULTICAST_ADDRESS, OSC_PORT); 
}

void draw() 
{
  background(255);
}


void oscEvent(OscMessage theOscMessage) 
{
  if (theOscMessage.checkAddrPattern("/Test")==true) {
    // the name of the message is test
    if (theOscMessage.checkTypetag("si")) {
      // type is string integer and float
      String one = theOscMessage.get(0).stringValue();  
      int two = theOscMessage.get(1).intValue(); 
      println(one,two);
    }
  }
}

