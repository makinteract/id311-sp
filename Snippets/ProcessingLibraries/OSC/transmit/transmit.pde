import oscP5.*;
import netP5.*;


OscP5 oscP5;
String MULTICAST_ADDRESS= "127.0.0.1"; //239.0.0.1";
int OSC_PORT= 5204;


void setup()
{ 
  size(100, 100);
  oscP5 = new OscP5(this, MULTICAST_ADDRESS, OSC_PORT);
}

void draw()
{
  background(255);
}

void mousePressed()
{
  sendMessage ("Hello", 5);
}



void sendMessage (String msg, int number)
{
  if (oscP5==null) return ; 
  OscMessage message = new OscMessage("/Test");
  message.add(msg);  
  message.add(number); 
  oscP5.send(message);
}


// void oscEvent(OscMessage theOscMessage) 
// {
//   if (theOscMessage.checkAddrPattern("/sp")==true) {
//     // the name of the message is test
//     if (theOscMessage.checkTypetag("ss")) {
//       // type is string integer and float
//       String one = theOscMessage.get(0).stringValue();  
//       String two = theOscMessage.get(1).stringValue(); 
//       println(one+" "+two);
//     }
//   }
// }






