import websockets.*;

WebsocketServer ws;
ArrayList<PVector> points;


void setup() {
	size(800, 600);
	points = new ArrayList<PVector>();

	ws = new WebsocketServer(this, 8025, "/pointInfo");
}

void draw() {
	background(0);

	fill(255);
	for (PVector p : points)
		ellipse(p.x, p.y, 10, 10);
}

void webSocketServerEvent(String msg) 
{
	println(msg);
	String[] tk = splitTokens(msg, ",");
	points.add(new PVector(parseInt(tk[0]), parseInt(tk[1])));
}

void mousePressed() {
	ws.sendMessage(mouseX + "," + mouseY);
}