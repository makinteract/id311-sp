import websockets.*;
import java.net.*;

WebsocketClient wsc;
ArrayList<PVector> points;

void setup(){
  size(800,600);

  points= new ArrayList<PVector>();
 
  	wsc= new WebsocketClient(this, "ws://localhost:8025/pointInfo");
  // wsc= new WebsocketClient(this, "ws://127.0.0.1:8025/pointInfo");
  
}

void draw(){
  background(255);
  fill(0);
  for (PVector p: points)
      ellipse(p.x, p.y, 10, 10);
}

void webSocketEvent(String msg){
 String[] tk= splitTokens(msg, ",");
 points.add(new PVector(parseInt(tk[0]), parseInt(tk[1])));
}


void mousePressed()
{
  println("send");
  
  try {
  	wsc.sendMessage(mouseX+","+mouseY);
  }catch (Exception e)
  {
  	println("Connection refused");
  }
  
}