class WekaData
{

  WekaData (String [] keys, String [] attributes)
  {
    this.attributes= attributes;
    this.keys= keys;
    reset();
  }


  void addForClassification (float [] input)
  {    
    if (inst == null) return;

    double[] vals = new double[inst.numAttributes()];
    vals[0] = 0; // default
    
    for (int i=0; i<input.length; i++)
    {
      vals[1+i]= input[i];
    }

    inst.add (new Instance(1.0, vals));
  }


  Instances getData()
  {
    return inst;
  }

  void reset()
  {
    FastVector atts = new FastVector();
    FastVector attVals = new FastVector();

    for (int i=0; i< attributes.length; i++)
    {
      attVals.addElement(attributes[i]);
    }

    atts.addElement(new Attribute("class", attVals));
    for (int i=0; i< attributes.length; i++)
    {
      atts.addElement(new Attribute(attributes[i]));
    }

    inst = new Instances("att", atts, 0);
    inst.setClassIndex(0);
  }

  
  String getKey (int index)
  {
     return keys[index];
  }


  private Instances inst;
  private String [] attributes;
  private String [] keys;
}

