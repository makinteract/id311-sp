// Weka
import weka.core.converters.*;
import weka.core.*;
import weka.classifiers.*;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.*;
import weka.classifiers.lazy.KStar;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.Logistic;
import java.io.*;

abstract class Weka
{

  Weka (String trainingFile)
  {
    this.trainingFile= trainingFile;
    initialize ();
  }


  // abstract methods
  abstract void initClassifier();
  abstract int getClassificationResult();



  synchronized protected boolean classify (WekaData data)
  {
    if (cls==null) return false;
    if (data==null) return false;
    Instances test= data.getData();
    if (test==null) return false;
    
    // else we are ready
    try {
      // create a model using the training data
      eval = new Evaluation(train);
      // classify
      eval.evaluateModel(cls, test);
      //println(eval.toSummaryString("\nResults\n======\n", false));
    }
    catch(Exception ex) {
      ex.printStackTrace();
      println(ex.getMessage());
    }
    
    return true;
  }



  // PRIVATE
 
   private void loadTrainingData (String trainingFile)
  {
    try
    {
      if (trainingFile.toLowerCase().endsWith(".csv"))
      {
        loadCSV (trainingFile);
      }
      else if (trainingFile.toLowerCase().endsWith(".arff"))
      {
        loadARFF (trainingFile);
      }
    }
    catch(Exception e) {
      println(e.getMessage());
    }
  }


  private void loadCSV (String trainingFile) throws IOException
  {
    CSVLoader loader = new CSVLoader();
    loader.setSource(new File(trainingFile));
    train = loader.getDataSet();
    train.setClassIndex(0);
  }

  private void loadARFF (String trainingFile)  throws IOException
  {
    FileReader reader = new FileReader(trainingFile); 
    train = new Instances(reader); 
    train.setClassIndex(0);
  }


  private synchronized void initialize ()
  {
    train=null; 
    // load training file
    loadTrainingData (trainingFile);
    // call the abstract method
    initClassifier ();
  }



  Instances train;
  Classifier cls=null;
  Evaluation eval=null;
  String trainingFile;
}





class NBWeka extends Weka
{

  NBWeka (String trainingFile)
  {
    super(trainingFile);
  }

  void initClassifier()
  {    
    try {
      cls= new NaiveBayes();
      cls.buildClassifier(train);
    }
    catch (Exception e)
    {
      println(e.getMessage());
    }
  }


  int getClassificationResult () 
  {
    if (cls==null || eval==null) return -1; // classifier not initialised

    FastVector predictions= eval.predictions();
    NominalPrediction np = (NominalPrediction) predictions.elementAt(0);
    int predicted= (int)np.predicted();
    int actual= (int)np.actual();
    return predicted;
  }
}

