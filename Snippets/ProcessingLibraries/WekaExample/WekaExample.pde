void setup()
{
  String [] keys= {"R","S"};
  String [] atts= {"Temperature", "Humidity"};
  float [] vals= {10, 70};
  
  WekaData data= new WekaData (keys, atts);
  data.addForClassification (vals);
  
  NBWeka w= new NBWeka (dataPath("weather.arff"));
  w.classify (data);
  int res= w.getClassificationResult ();
  println (data.getKey (res));
}



