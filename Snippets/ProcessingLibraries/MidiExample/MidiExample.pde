import themidibus.*; 

MidiBus myBus; 
boolean sustainOn=false;


void setup() {
  size(400, 400);
  background(0);

  MidiBus.list();             
  myBus = new MidiBus(this, 0, 1); // set input and output
}

void draw() {

}

void noteOn(int channel, int pitch, int velocity) {
  // Receive a noteOn
  println();
  println("Note On:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
}

void noteOff(int channel, int pitch, int velocity) {
  // Receive a noteOff
  println();
  println("Note Off:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
}

void controllerChange(int channel, int number, int value) {
  // Receive a controllerChange
  println();
  println("Controller Change:");
  println("--------");
  println("Channel:"+channel);
  println("Number:"+number);
  println("Value:"+value);
}



void mousePressed()
{
  int channel = 0;
  int pitch = (int) map(mouseX, 0, width, 50,70);
  int velocity = mouseY;

  myBus.sendNoteOn(channel, pitch, velocity); // Send a Midi noteOn
  delay(200);
  myBus.sendNoteOff(channel, pitch, velocity); // Send a Midi nodeOff
}

void mouseDragged()
{
  // pan = 10
  // controllers: http://nickfever.com/music/midi-cc-list
  myBus.sendControllerChange (0,10,(int) map(mouseX, 0, width, 0,127)); // pan left and right
}

void keyPressed()
{
  // sustain = 64
  if (sustainOn) myBus.sendControllerChange (0,64,127); //on 
  else myBus.sendControllerChange (0,64,0); //off
  sustainOn= !sustainOn;
}