import de.looksgood.ani.*;

float originalSize= 120;
float size = originalSize;
float scaleLarge= 3;

AniSequence seq;



void setup() {
  size(512, 512);
  smooth();
  noStroke();

  // you have to call always Ani.init() first!
  Ani.init(this);
  Ani.setDefaultEasing(Ani.CUBIC_IN_OUT);

  // Build a sequence
  seq = new AniSequence(this);
  seq.beginSequence();
  // animate the variables size in 3 sec after mouse is pressed
  seq.add (Ani.to(this, 3, "size", scaleLarge*originalSize));
  seq.add (Ani.to(this, 3, "size", originalSize, Ani.CUBIC_IN_OUT, "onEnd:startSequence"));
  seq.endSequence();
  
  // start
  startSequence();
}



void draw() {
  background(255);
  fill(0);
  rectMode(CENTER);
  rect(width/2, height/2, size, size, size/4);
}


void startSequence()
{
  delay(100); // waif before starting
  seq.start();
}