// Also works in Android

import controlP5.*;

ControlP5 cp5;
int buttonW = 120;
int buttonH = 60;
int spacer = 10;

void setup() {
  size (800, 600);
  //fullScreen(); // use this instead to use fullscreen

  cp5 = new ControlP5(this);

  // create a new button with name 'buttonA'
  cp5.addButton("One")
  .setPosition(width / 2 - buttonW - spacer, height / 2)
  .setSize(buttonW, buttonH);


  // and add another 2 buttons
  cp5.addButton("Two")
  .setPosition(width / 2 + spacer, height / 2)
  .setSize(buttonW, buttonH);


  // add a vertical slider
  cp5.addSlider("slider")
  .setLabel("My Slider")
  .setPosition(100, 100)
  .setSize(200, 20)
  .setRange(0, 200)
  .setValue(128)
  ;
}

void draw() {
  background(0);
}

public void One(int theValue) {
  println("Button One was pressed");
}

public void Two(int theValue) {
  println("Button Two was pressed");
}

void slider(float value) {
  println("The value: " + value);
}