PImage photo, maskImage;

void setup() {
  size(600, 402);
  photo = loadImage("a.jpg");
  maskImage = loadImage("b.jpg");
  photo.mask(maskImage);
}

void draw() {
  background(255);
  image(photo, 0, 0, width, height);
}