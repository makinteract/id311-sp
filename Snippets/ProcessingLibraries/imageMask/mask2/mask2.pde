PImage photo;
PGraphics maskImage;

void setup() {
  size(600, 402);
  photo = loadImage("a.jpg");
  
}

void draw() {
  background(255);

  maskImage = createGraphics (photo.width, photo.height);
  maskImage.beginDraw();
  maskImage.background(0);
  maskImage.fill(255);
  maskImage.ellipseMode(CENTER);
  maskImage.ellipse (mouseX,mouseY,100,100);
  maskImage.endDraw();

  photo.mask(maskImage);
  image(photo, 0, 0, width, height);
}


