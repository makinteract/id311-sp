import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.StatUtils;

void setup()
{
  double[] num={1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
  DescriptiveStatistics stat= new DescriptiveStatistics(num);
  println(stat.getMean());
  println(stat. getPercentile(50)); // median
  println(stat.getStandardDeviation());
  println(stat.getKurtosis());
  println(stat.getSkewness());
  println(stat.getSum());
  println(StatUtils.mode(num));
  println(StatUtils.sumSq(num));
}