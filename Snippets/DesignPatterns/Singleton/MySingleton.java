// The singleton class

public class MySingleton 
{
    private static final MySingleton singletonInstance = new MySingleton();

    private MySingleton() {
    }

    public static MySingleton Instance() {
        return singletonInstance;
    }
    
    public void printSingleton(){
        System.out.println("Calling the printSingleton of MySingleton");
    }
}