ArrayList<Watch> arr = new ArrayList<Watch>();
WatchFactory wf;

void setup()
{
	size(800,800);
	wf= new WatchFactory();
	arr.add (wf.getSportWatch(width/3, height/2, 100));
	arr.add (wf.getClassicWatch(2*width/3, height/2, 100));
}

void draw()
{
	background(255);
	for (Watch w:arr)
		w.draw();
}



class WatchFactory
{
	Watch getSportWatch (int x, int y, int sz)
	{
		Watch w;
		w= new Watch (x,y,sz);
		w.setTime (13,32,44);
		w.setBackgroundColor(color(128,68,128));
		w.setRefreshRate (10);
		return w;
	}

	Watch getClassicWatch (int x, int y, int sz)
	{
		Watch w;
		w= new Watch (x,y,sz);
		w.setTime (hour(),minute(),second());
		return w;
	}
}



class Watch extends Thread
{
	Watch (int x, int y, int size)
	{
		loc= new PVector(x,y);
		sz=size;
		start();
		bg= color(255);
		delay= DEFAULT_DELAY;
	}

	void setBackgroundColor (color c)
	{
		bg= c;
	}

	void setTime (int h, int m, int s)
	{
		hour= h;
		min= m;
		sec= s;
	}

	void setRefreshRate (int ms)
	{
		delay= ms;
	}

	void draw()
	{
		ellipseMode(CENTER);
		stroke(0);
		strokeWeight(sz/10);
		fill(bg);
		ellipse(loc.x, loc.y, sz*2, sz*2);
		drawhour(hour);
		drawMin(min);
		drawSecs(sec);
	}

	private void drawSecs (int sec)
	{
		float a= -PI/2 + 2*PI*sec/60.0;
		strokeWeight (sz/50);
		stroke(255,0,0);
		drawLineWithAngle(a, sz*4.0/5);
	}

	private void drawMin (int min)
	{
		float a= -PI/2 + 2*PI*min/60.0;
		strokeWeight (sz/30);
		stroke(0);
		drawLineWithAngle(a, sz*4.0/5);
	}

	private void drawhour (int h)
	{
		if (h>12) h-=12;
		float a= -PI/2 + 2*PI*h/12.0;
		strokeWeight (sz/20);
		stroke(0);
		drawLineWithAngle(a, sz*3.0/5);
	}


	private void drawLineWithAngle (float a, float length)
	{
		pushMatrix();
		translate(loc.x, loc.y);
		rotate(a);
		line(0,0,length,0);
		popMatrix();
	}




	void run()
	{
		while (true)
		{
			sec++;
			if (sec>=60) { min++; sec=0; }
			if (min>=60) { hour++; min=0; }
			if (hour>=24) hour=0;

			try 
			{
				Thread.sleep (delay);
			} catch (Exception e){}
		}
	}


	PVector loc;
	int sz;
	color bg;
	int hour, min, sec;
	int delay;
	public final int DEFAULT_DELAY = 1000; // 1 sec
}

