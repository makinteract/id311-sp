public class Person {
	private String lastName;
	private String firstName;
	private String middleName;
	private String salutation;
	private String streetAddress;
	private String city;
	private String state;
	private String country;

	public Person(
	    String newLastName,
	    String newFirstName,
	    String newMiddleName,
	    String newSalutation,
	    String newStreetAddress,
	    String newCity,
	    String newState,
	    String newCountry) {
		this.lastName = newLastName;
		this.firstName = newFirstName;
		this.middleName = newMiddleName;
		this.salutation = newSalutation;
		this.streetAddress = newStreetAddress;
		this.city = newCity;
		this.state = newState;
		this.country = newCountry;
	}

	String toString() {
		return  lastName + "," +
		        firstName + "," +
		        middleName + "," +
		        salutation + "," +
		        streetAddress + "," +
		        city + "," +
		        state + "," +
		        country;
	}
}




public class PersonBuilder {
	private String lastName;
	private String firstName;
	private String middleName;
	private String salutation;
	private String streetAddress;
	private String city;
	private String state;
	private String country;

	public PersonBuilder() {}

	public PersonBuilder lastName(String newLastName) {
		this.lastName = newLastName;
		return this;
	}

	public PersonBuilder firstName(String newFirstName) {
		this.firstName = newFirstName;
		return this;
	}

	public PersonBuilder middleName(String newMiddleName) {
		this.middleName = newMiddleName;
		return this;
	}

	public PersonBuilder salutation(String newSalutation) {
		this.salutation = newSalutation;
		return this;
	}

	public PersonBuilder streetAddress(String newStreetAddress) {
		this.streetAddress = newStreetAddress;
		return this;
	}

	public PersonBuilder city(String newCity) {
		this.city = newCity;
		return this;
	}

	public PersonBuilder state(String newState) {
		this.state = newState;
		return this;
	}

	public PersonBuilder country(String newCountry) {
		this.country = newCountry;
		return this;
	}


	public Person createPerson() {
		return new Person(lastName, firstName, middleName, salutation,
		                  streetAddress, city, state, country);
	}
}





void setup() {
	// Without builder
	Person a = new Person ("Bianchi", "Andrea", null, "PhD", "KAIST", "Daejeon", null, "ROK");
	Person b = new Person ("Jae", "Sungwoo", "Dobby", null, "Lab", null, null, null);
	println(a);
	println(b);

	// With builder
	PersonBuilder pb = new PersonBuilder();
	Person c = pb.firstName("Andrea").lastName("Bianchi").salutation("PhD").createPerson();
	println (c);
}









