void setup()
{
	Drink d= new Latte();
	d= new Whip (d);
	d= new Soy (d);

	println(d.getPrice());
}

abstract class Drink 
{
	abstract int getPrice();
}

class Americano extends Drink
{
	int getPrice(){ return 1000; }
}

class Latte extends Drink
{
	int getPrice(){ return 2000; }
}


abstract class Option extends Drink
{
	Drink baseDrink;

	Option (Drink d)
	{
		baseDrink= d;
	}

	// A reminder that this class is abstract 
	// so I do not need to implement the mentod
	// abstract int getPrice();
}


class Whip extends Option
{
	Whip (Drink d)
	{
		super (d);
	}

	int getPrice()
	{
		return 500 + baseDrink.getPrice();
	}
}

class Soy extends Option
{
	Soy (Drink d)
	{
		super (d);
	}

	int getPrice()
	{
		return 800 + baseDrink.getPrice();
	}
}