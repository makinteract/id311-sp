Receipt r= new Receipt();

void setup()
{
	r.cook();
}



class Receipt
{
	CookingState state;


	void cook()
	{
		setState (new Preparing());

		while (!(state instanceof Done))
		{
			delay(1000);			
			state.nextStep(this);
		}
	}

	void setState (CookingState s)
	{
		state= s;
	}
}


interface CookingState
{
	void nextStep(Receipt receit);
}

class Preparing implements CookingState
{
	void nextStep(Receipt receipt)
	{
		println("Preparing");
		receipt.setState (new Mixing());
	}
}

class Mixing implements CookingState
{
	void nextStep(Receipt receipt)
	{
		println("Mixing");
		receipt.setState (new Oven());
	}
}

class Oven implements CookingState
{
	void nextStep(Receipt receipt)
	{
		println("Oven");
		receipt.setState (new Check());	
	}
}

class Check implements CookingState
{
	void nextStep(Receipt receipt)
	{
		println("Check");
		if ((int)random(0, 2) % 2 == 0)
			receipt.setState (new Oven());
		else
			receipt.setState (new Serve());

	}
}

class Serve implements CookingState
{
	void nextStep(Receipt receipt)
	{
		println("Serve");
		receipt.setState (new Done());
	}
}

class Done implements CookingState
{
	void nextStep(Receipt receipt)
	{
	}
}



















