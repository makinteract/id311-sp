import java.util.*;

// Using the buit-in Observer pattern

class Fisherman implements Observer
{
	Fisherman(){  }

	public void update(Observable obs, Object param)
	{
		println(obs.getClass().getName());
		println(param.getClass().getName());
		println ((String)param);
	}
}

class Fish extends Observable
{
	String name;
	Fish(String name)
	{
		super();
		this.name= name;
	}

	void swim()
	{
		setChanged();
		notifyObservers(name);
	}
}

Fish f;
Fisherman fm;
void setup()
{
	f= new Fish("Nemo");
 	fm= new Fisherman();
	f.addObserver (fm);
}

void mousePressed()
{
	f.swim();
}

void draw(){}






