# ID311 Software Prototyping exercises, activities and snippets #

This includes a list of exercises, code-snippets and in-class-activities for software prototyping (ID311) at ID KAIST. *Latest update: Spring 2020.*

For explanations of code please refer to the class material uploaded in [KLMS](http://klms.kaist.ac.kr).


## Table of content

1. **Exercises**

	*Exercises with solutions*

	* [Recursion](Exercises/Recursion) 
		* Koch line
		* Tower of Hanoi (TOH)
		* Tree
	* [Classes](Exercises/Classes) 
		* Crazy buttons
		* SemaphoreClass
    * [Exception](Exercises/Exception)
        * MyParseInt
    * [Inheritance](Exercises/Inheritance)
		* Cars
		* Shapes
	* [Polymorphism](Exercises/Polymorphism)
		* Animals
		* ButtonsExercise
		* ButtonsExercise
	* [Design Patterns](Exercises/DesignPatterns)
		* BallFactory
		* LightSwitchObserver
		* FakeSensorObserver
	* [Files](Exercises/Files)
		* SavingCSV
		* SavingJSON
	* [p5.js](Exercises/p5js)
		* pixelGraphics
		* csv
		* AnalogWatches		

2. **Activities**

	*In class activity which are graded*

	* [Activity 1](Activities/LightSwitch): intro to classes and objects
	* [Activity 2](Activities/AnalogWatch): inheritance and threads	
	* [Activity 3](Activities/Salaries): polymorphism and interfaces
	* [Activity 4](Activities/JSONrects): JSON file parsing
	* [Activity 5](Activities/WebSockets): WebSockets and JSON
	* [Activity 6](Activities/HOfunctions): High order functions

3. **Snippets**

	*Code for your own reference*

	* [Recursion](Snippets/Recursion) 
		* UpperCase
	* [Threads](Snippets/Threads)
		* WebThread
		* Animation with squares
		* AnalogWatchInterface (AnalogWatch + interface)
	* [Design Patterns](Snippets/DesignPatterns)
		* Builder
		* Decorator
		* Factory
		* Observer
		* Singleton
		* StateMachine
	* [Files](Snippets/Files)
		* sqliteExample
		* XMLrects
	* [Processing Libraries](Snippets/ProcessingLibraries)
		* Ani Basics
		* ControlP5 GUI
		* Descriptive Stats
		* Firmata
		* GestureRecognition
		* ImageMask
		* Midi
		* OSC
		* WebSockets
		* Weka
	* [p5js](Snippets/p5js)
		* WebSockets
		* MenuGUI
		* P5 gui
	* [WebSocketsDebuggingTools](Snippets/WebSocketsDebuggingTools)
		* WS_Server
		* WS_Client
		* WS_jsClient