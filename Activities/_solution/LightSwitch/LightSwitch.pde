ArrayList<Light> allLights= new ArrayList<Light>();

Switch s1, s2, s3;

void setup()
{	
	size (800, 600);

	allLights.add (new Light(width/2, 50));
	for (int i=0; i<3; i++)
	{
		allLights.add (new Light(width/2+i*120, 90));
		allLights.add (new Light(width/2-i*120, 90));
	}

	s1= new Switch (width/3, 50);
	s2= new Switch (width/2, 50);
	s3= new Switch (2*width/3, 50);

	for (Light l: allLights)
	{
		switch ((int)random(0, 3)) {
		    case 0: s1.addLight (l); break;
		    case 1: s2.addLight (l); break;
		    case 2: s3.addLight (l); break;
		}
	}
	
}

void draw()
{
	background(255);
	for (Light l: allLights) l.draw();
	s1.draw();
	s2.draw();
	s3.draw();
}

void mousePressed()
{
	s1.swithOnOff();
	s2.swithOnOff();
	s3.swithOnOff();
}


class Light
{
	private boolean onState;
	private int x, y;
	private int w, h;
	private PImage [] imgs;

	Light (int x, int width)
	{
		imgs= new PImage [2];
		imgs[0]= loadImage ("lightOff.jpg");
		imgs[1]= loadImage ("lightOn.jpg");
		w= width;
		float ratio= float(imgs[0].height)/imgs[0].width;
		h= int(w*ratio);

		this.x= x;
		this.y= h/2;
		onState= false;
	}	

	void toggle (){ onState= !onState; }
	boolean isOn (){ return onState; }

	void draw()
	{
		imageMode(CENTER);
		if (onState) image(imgs[1], x, y, w, h);
		else image(imgs[0], x, y, w, h);
	}
}



class Switch 
{
	private PImage imgs[];
	private boolean onState;
	private int x, y;
	private int w, h;
	private ArrayList<Light> lights;

	Switch (int x, int width)
	{
		imgs= new PImage [2];
		imgs[0]= loadImage ("switchOff.jpg");
		imgs[1]= loadImage ("switchOn.jpg");
		w= width;
		float ratio= float(imgs[0].height)/imgs[0].width;
		h= int(w*ratio);

		this.x= x;
		y= height-h/2;

		lights= new ArrayList<Light>();
	}

	void addLight (Light light)
	{
		lights.add (light);
	}

	void draw()
	{
		imageMode(CENTER);
		if (onState) image(imgs[1], x, y, w, h);
		else image(imgs[0], x, y, w, h);
	}

	boolean isOver()
	{
		if (mouseX < x-w/2 || mouseX > x+w/2) return false;
		if (mouseY < y-h/2 || mouseY > y+h/2) return false;
		return true;
	}

	void swithOnOff()
	{
		if (!isOver()) return;
		onState= !onState;
		for (Light l: lights)
			l.toggle();
	}
}





