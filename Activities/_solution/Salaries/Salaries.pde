interface Employable
{
	int getSalary();
}

class Faculty implements Employable
{
	int getSalary()
	{
		return 80;
	}
}


class Staff implements Employable
{
	int getSalary()
	{
		return 75;
	}
}


class Student
{
	boolean scholarship;
	Student()
	{
		// 50-50 chance to get a scholarship
		scholarship= random(100) < 50;
	}

	boolean hasScholarship()
	{
		return scholarship;
	}
}

class GradStudent extends Student implements Employable
{
	
	GradStudent()
	{
		super();	
	}

	int getSalary()
	{
		if (hasScholarship()) return 45;
		return 35;
	}
}


class School 
{
	ArrayList<Employable> employees;

	School()
	{
		employees= new ArrayList<Employable>();
	}

	void addEmploee(Employable e)
	{
		if (employees==null || e==null) return;
		employees.add(e);
	}

	float getAverageSalary()
	{
		float result =0;
		if (employees==null ) return result;

		for (Employable e: employees)
		{
			result+= e.getSalary();
		}

		return result / employees.size();

	}
}




void setup()
{
	School KAIST= new School();
	KAIST.addEmploee (new Faculty());
	KAIST.addEmploee (new Faculty());
	KAIST.addEmploee (new Faculty());
	KAIST.addEmploee (new Faculty());
	KAIST.addEmploee (new GradStudent());
	KAIST.addEmploee (new GradStudent());
	KAIST.addEmploee (new GradStudent());
	KAIST.addEmploee (new GradStudent());
	KAIST.addEmploee (new Staff());
	KAIST.addEmploee (new Staff());
	KAIST.addEmploee (new Staff());
	KAIST.addEmploee (new Staff());

	println (KAIST.getAverageSalary());
}
