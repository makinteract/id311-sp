ArrayList<Rect> rectangles;

void setup()
{
	size(800, 600);
	rectangles= new ArrayList<Rect>();
	loadJsonData ("data/rectangles.json");
}

void draw()
{
	background(120);
	for (Rect r: rectangles) r.draw();
}


void loadJsonData(String filename)
{
	JSONObject json = loadJSONObject(filename);
	JSONArray rects = json.getJSONArray("rects");
	for (int i = 0; i < rects.size(); i++) 
	{
    	JSONObject rect= rects.getJSONObject(i); 
    	rectangles.add (new Rect(rect));
    }
}


class Rect
{
	private int x, y, w, h;
	private color c;
	private boolean centered;

	Rect (JSONObject rect)
	{
		x = rect.getInt("x");
    	y = rect.getInt("y");
    	w = rect.getInt("width");
    	h = rect.getInt("height");
    	int r = rect.getInt("r");
    	int g = rect.getInt("g");
    	int b = rect.getInt("b");
    	c= color (r, g, b);
    	centered = rect.getBoolean("center");
	}

	Rect (int x, int y, int w, int h, color c, boolean centered)
	{
		this.x=x; this.y=y; this.w=w; this.h=h;
		this.c=c; this.centered=centered;
	}

	void draw()
	{
		if (centered) rectMode(CENTER);
		else rectMode(CORNER);
		fill(c);
		noStroke();
		rect(x,y,w,h);
	}
}