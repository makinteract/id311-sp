Watch watch;

void setup()
{
	size(800,800);
	watch= new Watch (width/2, height/2, 300);
	watch.setBackgroundColor(color(147,90,142));
	watch.setTime (hour(), minute(), second());
	watch.setRefreshRate(50);
}

void draw()
{
	background(255);
	watch.draw();
}


class Watch extends Thread
{
	Watch (int x, int y, int size)
	{
		loc= new PVector(x,y);
		sz=size;
		start();
		bg= color(255);
		refreshDelay= DEFAULT_DELAY;
	}

	void setBackgroundColor (color c)
	{
		bg= c;
	}

	void setTime (int h, int m, int s)
	{
		hour= h;
		min= m;
		sec= s;
	}

	void setRefreshRate (int ms)
	{
		refreshDelay= ms;
	}

	void draw()
	{
		ellipseMode(CENTER);
		stroke(0);
		strokeWeight(sz/10);
		fill(bg);
		ellipse(loc.x, loc.y, sz*2, sz*2);
		drawhour(hour);
		drawMin(min);
		drawSecs(sec);
	}

	private void drawSecs (int sec)
	{
		float a= -PI/2 + 2*PI*sec/60.0;
		strokeWeight (sz/50);
		stroke(255,0,0);
		drawLineWithAngle(a, sz*4.0/5);
	}

	private void drawMin (int min)
	{
		float a= -PI/2 + 2*PI*min/60.0;
		strokeWeight (sz/30);
		stroke(0);
		drawLineWithAngle(a, sz*4.0/5);
	}

	private void drawhour (int h)
	{
		if (h>12) h-=12;
		float a= -PI/2 + 2*PI*h/12.0;
		strokeWeight (sz/20);
		stroke(0);
		drawLineWithAngle(a, sz*3.0/5);
	}


	private void drawLineWithAngle (float a, float length)
	{
		pushMatrix();
		translate(loc.x, loc.y);
		rotate(a);
		line(0,0,length,0);
		popMatrix();
	}




	void run()
	{
		while (true)
		{
			sec++;
			if (sec>=60) { min++; sec=0; }
			if (min>=60) { hour++; min=0; }
			if (hour>=24) hour=0;

			try 
			{
				Thread.sleep (refreshDelay);
			} catch (Exception e){}
		}
	}


	private PVector loc;
	private int sz;
	private color bg;
	private int hour, min, sec;
	private int refreshDelay;
	public final int DEFAULT_DELAY = 1000; // 1 sec
}

