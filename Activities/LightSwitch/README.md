# Activity 1: classes

Create the code to turn on and off lights using switches.

![](data/demo.gif)

**Part 1**: create the class Light. It should have the following methods:

![](data/classLight.png)


Things to consider:

    * what members do you need?
    * getters and setters?
    * which constructor? Default or custom?
     
**Part 2**: create the class Switch with the following methods:

![](data/classSwitch.png)

Things to consider:

    * where to keep the instances of Light inside the Switch class? Array? ArrayList?

**Part 3**: glue everything together.

    * Create the lights
    * Create the switches
    * Associate lights to switches
    * Toggle switches at mousePressed