# Activity 6: High-Order functions

In this activity you will have to extract data from the **dataJSON** array, in order to answer the following questsions.

1. Add a random income between 10 and 100 to each person, except me (the last person), who has income of 100 **😀😀😀 thanx ❤️❤️❤️**

2. Get the list of people with age > 21. How many are there?

3. Get the _total_ and _average_ income for the entire population.

4. Get total and average income of people older than 21 (included).

5. Find in which city "Mitsue Tollner" lives.

6. Find if there is anyone who lives in the same city of others.

7. Get the city/cities where most people live.

