# Activity 4: JSON file parsing

1) Create a JSON file containing the description of several rectangles that you will have to draw.

![](data/example.png)

2) Each rectangle should be describe with the following properties:
    - **x** and **y** coordinates
    - **width** and **height** for size
    - color presented as **r**, **g**, **b** values
    - **center** property that describe whether to use the center as reference point for drawing (when *true*) or the corner (when *false*)   

3) Create a code that loads such JSON file and display the two rectangles. Pass the **JSONObject** to the constructor of the Rect class.





