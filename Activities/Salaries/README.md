# Activity 3: polymorphism and interfaces

**Part 1**:
use this **incomplete** UML diagram to create the code for the classes *Employee*, *Faculty*, *Staff*, *GradStudent* and *School*. In your main craete a school (e.g., KAIST) and assign to it faculty, staff and grad students members. 

![](data/uml1.png)

Compute the total amount of salary that the school has to pay to its employees. Use the following table as fictional data source. Please note that a student  student has a fix salary of $35, but if she/he has also a scholarship, the salary is $45 (+$10 scholarship). Scholarship are randomly assigned with a 50-50 chance.


|| Faculty     | Staff    | GradStudents |
|--------| --------|---------|-------|
| Salary | $80  | $75   | $35 (+$10 scholarship)   |


**Part 2**:
modify the previous code by adding the class Student (parent class of GradStudent) and using instead of a concrete base class an interface. Student has a method called *payTuition* (which does nothing but printing on the console "I pay!"). Also note that scholarships could also be given to normal students, hence the previous table become:

|| Faculty     | Staff    | Students | GradStudents |
|--------| --------|---------|-------|-------|
| Salary | $80  | $75   | (+$10 scholarship)  | $35 (+$10 scholarship)  |

Adjust your code in the main.

![](data/uml2.png)
