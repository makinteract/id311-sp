# Activity 5: WebSockets and JSON

![](data/classes.png)

## Part 1: Drawing shapes

This first part is very straightforward and simply requires you to generate shapes using a factory. 

Here an example of rectangular shape (*Rect*) expressed using the JSON format:

```
{
	"shape": "rect",
	"x": 100,
	"y": 200,
	"w": 50,
	"h": 40,
	"r": 100,	
	"g": 120,	
	"b": 200
}
```
Here another JSON snippets with an example for a *Circle* shape.

```
{
	"shape": "circle",
	"x": 100,
	"y": 200,
	"diam": 50,
	"r": 0,	
	"g": 120,	
	"b": 200
}
```
Using these snippets your code should creates the corresponding shapes in the shapes ArrayList and draw them.


## Part 2: WebSockets

You should create a **Websocket** server on the **localhost**, with port **8025** and URI **/shapeInfo**. 

Then connect a client to the server and send the JSON string that describes a shape. Your sever code should handle the incoming event (*webSocketServerEvent(String inputString)*), parse the input string as JSON and extract the information necessary to create the shape (i.e., create a JSONObject from the string received, and pass the whole object to the constructor of Rect and Circle, using the factory method you created in part 1).

For testing your code, you do *not* need to implement the Client side, instead you can use any of these two options.

* Use the [WSClient](./data/WSClient) sketch I wrote for you
* Use the [Smart Websocket Client extension](https://chrome.google.com/webstore/detail/smart-websocket-client/omalebghpgejjiaoknljcfmglgbpocdp?hl=en) for Chrome and set the address to: *ws://localhost:8025/shapeInfo*
