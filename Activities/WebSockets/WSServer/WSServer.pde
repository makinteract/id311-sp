/*
{
	"shape": "rect",
	"x": 100,
	"y": 200,
	"w": 50,
	"h": 40,
	"r": 100,	
	"g": 120,	
	"b": 200
}

{
	"shape": "circle",
	"x": 100,
	"y": 200,
	"diam": 50,
	"r": 0,	
	"g": 120,	
	"b": 200
}
*/


/*
	Websocket address: ws://localhost:8025/shapeInfo
	To test your code, feel free to use either
		1) WSClient
		2) Smart Websocket Client extension for Chrome (https://chrome.google.com/webstore/detail/smart-websocket-client/omalebghpgejjiaoknljcfmglgbpocdp?hl=en)

*/

import websockets.*;

// Your Shape data
ArrayList<Shape> shapes;
ShapeFactory sf;

// The server
WebsocketServer ws;

void setup() {
	size(800, 600);
	shapes = new ArrayList<Shape>();
}

void draw() {
	background(50, 109, 168);
	for (Shape s: shapes) println("draw the shape");
}




class ShapeFactory
{
	ShapeFactory(){}

	// ...
}


abstract class Shape
{

}




