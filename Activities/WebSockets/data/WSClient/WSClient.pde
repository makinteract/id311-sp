import websockets.*;
import java.net.*;
import controlP5.*;

WebsocketClient wsc;
ControlP5 cp5;
RadioButton rb;
Slider2D s;

String shapeType = "";
int x, y, r, g, b;
final int SIZE = 50;


void setup() {
  size(800, 600);
  wsc = new WebsocketClient(this, "ws://localhost:8025/shapeInfo");

  // GUI
  cp5 = new ControlP5(this);

  cp5.addSlider("sliderR")
  .setLabel("R")
  .setPosition(100, 100)
  .setSize(200, 20)
  .setRange(0, 255)
  .setValue(128)
  ;

  cp5.addSlider("sliderG")
  .setLabel("G")
  .setPosition(100, 130)
  .setSize(200, 20)
  .setRange(0, 255)
  .setValue(128)
  ;

  cp5.addSlider("sliderB")
  .setLabel("B")
  .setPosition(100, 160)
  .setSize(200, 20)
  .setRange(0, 255)
  .setValue(128)
  ;

  rb = cp5.addRadioButton("radioButton")
       .setPosition(100, 50)
       .setSize(40, 20)
       .setColorForeground(color(120))
       .setColorActive(color(255))
       .setColorLabel(color(255))
       .setItemsPerRow(2)
       .setSpacingColumn(50)
       .addItem("Rect", 0)
       .addItem("Circle", 1)
       ;


  s = cp5.addSlider2D("location")
      .setPosition(100, 200)
      .setSize(400, 300)
      .setMinMax(0, 0, 800, 600)
      .setValue(800, 600)
      //.disableCrosshair()
      ;

  cp5.addButton("createNewShape")
  .setLabel ("Create")
  .setPosition(350, 50)
  .setSize(150, 100);
}


void draw() {
  background(0);
}


// GUI control

void controlEvent(ControlEvent theEvent) {
  if (theEvent.isFrom(rb)) {
    int selection = (int)(theEvent.getValue());
    if (selection < 0) return;
    // 0 => rect, 1 => circle
    if (selection == 0) shapeType = "rect";
    else shapeType = "circle";
  } else if (theEvent.isFrom(s)) {
    x = (int)s.getArrayValue()[0];
    y = (int)s.getArrayValue()[1];
  }
}

void createNewShape(int theValue) {

  JSONObject json = new JSONObject();
  json.setString("shape", shapeType);
  json.setInt("x", x);
  json.setInt("y", y);
  json.setInt("r", r);
  json.setInt("g", g);
  json.setInt("b", b);
  if (shapeType.equals("rect")) {
    json.setInt("w", SIZE);
    json.setInt("h", SIZE);
  } else {
    json.setInt("diam", SIZE);
  }

  try {
    wsc.sendMessage(json.toString()); // convert to string
  } catch (Exception e) {
    println("Connection refused");
  }
}

void sliderR (float value) {
  r = (int)value;
}

void sliderG (float value) {
  g = (int)value;
}

void sliderB (float value) {
  b = (int)value;
}





