# Activity 2: inheritance and threads

Create the code for a working analog watch.

![](data/watch.gif)

**Part 1**: create the class Watch. It should have the following members and methods:

![](data/watchUML.png)

Things to consider:

* use **translate** and **rotate** to draw the watch's arms at the correct orientations
     
**Part 2**: extends from Thread and add the animation effect

![](data/allUML.png)

Things to consider:

* use Thread.sleep(**milliseconds**) to crate a delay. To use this method you need to surround it in a try/catch block.

