Watch watch;

void setup()
{
	size(800,800);

	// Uncomment to test
	// watch= new Watch (width/2, height/2, 300);
	// watch.setBackgroundColor(color(147,90,142));
	// watch.setTime (hour(), minute(), second());
	// watch.setRefreshRate(50);
}

void draw()
{
	background(255);

	// Uncomment to test
	// watch.draw();
}


class Watch 
{
	
	// A constant member
	public final int DEFAULT_DELAY = 1000; // 1 sec
}

